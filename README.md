[![SEAgle_logo_net_300x79.png](https://bitbucket.org/repo/B4y9e5/images/586573375-SEAgle_logo_net_300x79.png)
](http://se.uom.gr/seagle)

# What is this? #
## Seagle is an online platform for Software Repository Mining, and evolution analysis of JAVA projects. It facilitates the collection of project related data, organizes relevant information in comprehensible reports and provides a useful tool for empirical research. ##

# Overview #
## The development of the platform was driven by the following key issues: ##
* the platform should be easy to use. To this end we opted for a Web based platform enabling users to analyze a repository by a single click (either selection of an already analyzed project or by providing the git repository URI).
* software repositories encompass a project’s history. As a result, all reported information spans across all available versions, i.e. constitutes a form of software evolution analysis.
* Any software system has several facets. Therefore, we offer multiple views concerning commit-related metrics, source code metrics and graph based metrics.
* Empirical studies very often focus on the investigation of relations among variables. To satisfy this need we offer direct correlation analysis between any two monitored variables. (for this reason the x-axis is common on all diagrams and represents software versions)
* Contemporary software repositories are extremely large in size. To confront this challenge, we optimized the process of extracting commit-related metrics, which are demanding since they involved the analysis of thousands of commits.


# Build Instructions #

*We suggest that the best way to extend Seagle is to fork this repo, work on your own repository freely, experimentize on everything you want, and when you are sure that your code works, create a pull request.*

Seagle is comprised of multiple components. In this repository lies the server component that handles the repository cloning, performs all the metric calculations and stores the results in the database.

If you what to fork seagle-server and develop your own analysis or metric you can follow the following procedure. We hope you will find it easy.



# Internal dependencies #

Seagle need an in-house built library. So it should be cloned and built separately.

Clone this repository [https://github.com/teohaik/seagle-api](https://github.com/teohaik/seagle-api)

navigate to its folder and build it by running 


```
#!java

mvn install
```

if build fails, try building with test skipping:


```
#!java

mvn install -Dmaven.test.skip=true
```


# Java Version #

Seagle is extensively tested and developed in Java 7, but it also runs smoothly in Java 8.


# Application Server #

The recommended application server is the Payara Server. It is a modified and well maintained release of the Glassfish. 

You can download Payara from [here.](http://www.payara.fish/)


You can also use pure Glassfish @ version 4.1 which is extensively tested and stable.

**BUT BE CAREFULL!**

Netbeans comes with the latest Glassfish (ver 4.1.1) embedded. However this version **exhibits a major bug**, which until this day (5 Aug 2016) is unresolved. 
So, Glassfish 4.1.1 needs a modification in an internal library in order to work properly. More info [here.](https://java.net/jira/browse/GLASSFISH-21440) 

If you want to use Glassfish version 4.1.1 you should do the following:

1. Go to GLASSFISH_INSTALL_DIR/glassfish/modules/

2. Locate module with name: org.eclipse.persistence.moxy.jar

3. Open moxy module with winrar

4. Go to the folder META-INF and open the file MANIFEST.MF with your favorite text editor and add this line in the Import-Package section:


```
#!java
,org.xml.sax.helpers,javax.xml.parsers;resolution:=optional,javax.naming;resolution:=optional

```

You should also increase the Xms, Xmx and MaxPermSize JVM variables to values as high as your server allows:



```
#!java

-XX:MaxPermSize=10g
-Xms7g
-Xmx10g

```




then, you are ready to start using Glassfish.






# Seagle configuration files #

Seagle is fully configured by its config files which initially lie in the folder: ~CODE_ROOT/src/main/resources/**seagle**

Copy the seagle folder to your home directory. For example if you use windows this is probably the 


```
#!bash

C:\Users\user-name\seagle
```


or in linux-based systems:


```
#!bash

/home/user-name/seagle
```


You can modify the initial config files which lie in the *config* directory, especially the file: *seagle_domain.properties
*
There you can define a Gmail Account for Email notification and also define your desired folder names for the project repositories and the source code that will be downloaded.


## Database ##
Seagle is heavily depended on a Database. So if you want to develop locally you should first download an install MySql Server Community Edition. 

For the easy creation of databases, data and user manipulation we propose the MySql Workbench as it is a nice and free graphical database management system. 

A good choice is to download the MySql Installer which is an easy to use installation guide. It contains custom options where you can select what products you want to install. Just make sure to check "MySql Server" and "MySql Workbench"

Just after the installation you will be asked to create a **root** account for the administration of the database. Create the account and make sure that you have **noted down the root password!**

Create a database with the name: 
```
#!sql

seagle2db
```
Create a user with the name: 
```
#!sql

seagleuser
```
user password: 


```
#!sql

Se@gle_2016@MySql
```

then give the user "seagleuser" privilleges to read, write and create tables on the seagle2db


```
#!sql

GRANT ALL PRIVILEGES ON seagle2db.* TO 'seagleuser'@'%';
```


or by using the MySql Workbench:

![mysql-workbench-tutorial-1.PNG](https://bitbucket.org/repo/B4y9e5/images/238664673-mysql-workbench-tutorial-1.PNG)

and for granting privileges

![mysql-workbench-tutorial-2.PNG](https://bitbucket.org/repo/B4y9e5/images/1757949592-mysql-workbench-tutorial-2.PNG)


## Environment Variable ##

As a final step, you should set the path of your desired seagle home folder location as an environment variable under the name: **seagleHome**

Example (Linux)

```
#!bash

echo $seagleHome

/home/chaikalis/seagle

```



You are now ready to build and run [Seagle!](http://se.uom.gr/seagle/)

OR

#[Visit Seagle's website](http://se.uom.gr/seagle/)#