/**
 * 
 */
package gr.uom.java.seagle.v2.scheduler;

import static org.junit.Assert.*;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.se.util.concurrent.TaskType;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.concurrent.ManagedExecutorService;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class SeagleTaskSchedulerManagerTest extends AbstractSeagleTest {

   @EJB
   SeagleManager seagleManager;

   @Resource
   ManagedExecutorService service;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }
   
   static Logger logger = Logger.getLogger(SeagleTaskSchedulerManagerTest.class.getName());

   public void wakeUpExecutor() throws InterruptedException, ExecutionException {
      // This should be called as a workaround
      // to wake up the executor to execute the Consumer threads of
      // task scheduler!!! (shit this is arquillian & GF))
      service.submit(new Runnable() {
         
         @Override
         public void run() {
            try {
               logger.log(Level.INFO, "waking up thread pool");
               Thread.sleep(10);
               logger.log(Level.INFO, "thread pool woke up");
            } catch (InterruptedException e) {
               logger.log(Level.SEVERE,
                     "Could not wake up executor");
            }
         }
      }).get();
   }

   @Test
   public void testScheduler() throws InterruptedException, ExecutionException {

      TaskSchedulerManager scheduler = resolveScheduler();
      wakeUpExecutor();
      TaskType heavyTasks = scheduler.getType("serialTask");
      TaskType moderateTasks = scheduler.getType("parallelTask");
      TaskType quickTasks = scheduler.getType("quickTask");

      AtomicInteger threads = new AtomicInteger(0);
      AtomicInteger achievedMaxThreads = new AtomicInteger(0);
      AtomicInteger started = new AtomicInteger(0);
      AtomicInteger ended = new AtomicInteger(0);
      int maxThreads = 7;
      int iterations = 20;
      
      for (int i = 0; i < iterations; i++) {
         // Create 3 heavy (single running) tasks)
         createRequest(scheduler, heavyTasks, 3, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, moderateTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, quickTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 3 heavy (single running) tasks)
         Thread.sleep(13);
         createRequest(scheduler, heavyTasks, 3, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, moderateTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, quickTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
         Thread.sleep(10);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, moderateTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 3 heavy (single running) tasks)
         createRequest(scheduler, heavyTasks, 3, started, ended, threads,
               achievedMaxThreads, maxThreads);
         // Create 10 moderateTasks (single running) tasks)
         createRequest(scheduler, quickTasks, 10, started, ended, threads,
               achievedMaxThreads, maxThreads);
      }
      scheduler.shutdown();

      logger.log(Level.INFO,"Started: " + started);
      logger.log(Level.INFO,"Ended: " + ended);
      logger.log(Level.INFO,"Max threads achieved: " + achievedMaxThreads);
      assertEquals(iterations*69, started.get());
      assertEquals(iterations*69, ended.get());
      assertEquals(maxThreads, achievedMaxThreads.get());
   }

   private void createRequest(TaskSchedulerManager scheduler, TaskType type,
         int numOfTasks, AtomicInteger started, AtomicInteger ended,
         AtomicInteger threads, AtomicInteger achievedMaxThreads, int maxThreads)
         throws InterruptedException, ExecutionException {

      for (int i = 0; i < numOfTasks; i++) {
         Task task = new Task(started, ended, threads, achievedMaxThreads,
               maxThreads);

         scheduler.schedule(task, type);
      }
   }

   private TaskSchedulerManager resolveScheduler() {
      TaskSchedulerManager manager = seagleManager
            .getManager(TaskSchedulerManager.class);
      assertNotNull(manager);
      return manager;
   }
}
