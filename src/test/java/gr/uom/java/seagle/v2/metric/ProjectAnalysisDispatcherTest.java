package gr.uom.java.seagle.v2.metric;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisEventType;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.MetricFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.ProjectFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.context.ContextManager;
import gr.uom.se.util.event.DefaultEvent;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.analysis.version.provider.BranchTagProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedTagVersionNameProvider;
import gr.uom.se.vcs.analysis.version.provider.ConnectedVersionProvider;
import gr.uom.se.vcs.analysis.version.provider.TagProvider;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;

/**
 *
 * @author elvis
 */
public class ProjectAnalysisDispatcherTest extends AbstractSeagleTest {

    public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";

    @Inject
    private SeagleManager seagleManager;

    @Inject
    private ProjectFacade projectFacade;

    @Inject
    private MetricFacade metricFacade;

    @Inject
    private ProjectManager projectManager;

    @Inject
    private MetricManager metricManager;

    @Inject
    private EventManager eventManager;

    @Deployment
    public static JavaArchive deploy() {
        return AbstractSeagleTest.deployment();
    }

    static boolean init = false;

    @Test
    public void init() throws VCSRepositoryException {

        Collection<Metric> previousMetrics = metricManager.getMetrics();

        // Activate the test metric first
        metricManager.activateMetric(TestMetric.class);
        // download a project
        projectManager.clone(purl1);
        // register the metric to project
        // Get metric first
        List<Metric> list = metricFacade.findByMnemonic(TestMetric.mnemonic);
        assertEquals(1, list.size());
        Metric metric = list.get(0);

        // Get project
        List<Project> plist = projectFacade.findByUrl(purl1);
        assertEquals(1, plist.size());
        Project project = plist.get(0);
        // Register metric to project
        metricManager.registerMetricToProject(project, metric);
        // Remove all previous metrics from project
        metricManager.removeMetricFromProject(project, previousMetrics);

        // Now create the test metric and set it to listen to events
        TestMetric testMetric = new TestMetric(eventManager);

        eventManager.addListener(AnalysisEventType.ANALYSIS_COMPLETED, testMetric);
        eventManager.addListener(MetricRunEventType.START, testMetric);

        // Trigger the event
        trigerArtificialEvent();

        assertNotNull(testMetric.beforeInfo);
        assertNotNull(testMetric.afterInfo);

        // Now remove the previous metric from DB
        metricManager.removeMetricFromProject(project, metric);
        metricFacade.remove(metric);

        // Delete the project
        projectManager.delete(purl1);
    }

    private void trigerArtificialEvent() throws VCSRepositoryException {
        // Now we should create an artificial event
        // Start first the list of commit versions
        VCSRepository repo = projectManager.getRepository(purl1);
        ConnectedVersionProvider versionProvider = selectVersionProviderOfTheBranchWithMaxNumberOfTags(repo);
        try {
            for (VCSCommit c : versionProvider) {
                projectManager.checkout(repo, c);
            }
        } finally {
            if (repo != null) {
                repo.close();
            }
        }

        AnalysisRequestInfo info
                = new AnalysisRequestInfo("elvis@ligu.com", purl1);
        Event event = new DefaultEvent(
                AnalysisEventType.ANALYSIS_REQUESTED,
                new EventInfo(info, new Date()));
        eventManager.trigger(event);
    }

    public ConnectedVersionProvider selectVersionProviderOfTheBranchWithMaxNumberOfTags(VCSRepository repo) throws VCSRepositoryException {
        Collection<VCSBranch> branches = repo.getBranches();
        ConnectedVersionProvider maxVersionProvider = null;
        int maxVersionCount = 0;

        ContextManager cm = seagleManager.getManager(ContextManager.class);

        ModuleContext context = cm.lookupContext(repo, ModuleContext.class);
        ConnectedVersionProvider versionProvider = context.load(ConnectedVersionProvider.class);

        for (VCSBranch branch : branches) {
            TagProvider tagProvider = new BranchTagProvider(repo, branch.getName());

            int currentVersionCount = versionProvider.getCommits().size();

            if (currentVersionCount > maxVersionCount) {
                maxVersionProvider = versionProvider;
                maxVersionCount = currentVersionCount;
            }
        }
        return maxVersionProvider;
    }
}
