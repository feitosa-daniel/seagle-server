package gr.uom.java.seagle.v2.project;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import gr.uom.java.seagle.v2.AbstractSeagleTest;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.SeaglePathConfig;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Theodore & Elvis
 */
public class ProjectManagerBeanTest extends AbstractSeagleTest {

   public static final String purl1 = "https://github.com/fernandezpablo85/scribe-java.git";

   @Inject
   private SeagleManager seagleManager;

   @Inject
   private ProjectManager projectManager;

   @Inject
   private EventManager eventManager;

   private static boolean initialized = false;
   // This is required in the end of test tear down
   private static SeaglePathConfig seaglePathConfig;

   @Deployment
   public static JavaArchive deploy() {
      return AbstractSeagleTest.deployment();
   }

   private static final EventListener myListener = new EventListener();

   @AfterClass
   public static void tearDown() throws IOException {
      assertNotNull(seaglePathConfig);
      // Now cleanup the folder
      Path home = seaglePathConfig.getDataPath();
      FileUtils.deleteDirectory(home.toFile());
   }

   @Before
   public void initSeagleManager() throws IOException {
      if (!initialized) {
         SeaglePathConfig pathConfig = seagleManager.getSeaglePathConfig();

         // We must ensure that all data are removed before the
         // tests begin, otherwise it will throw errors
         Path dataPath = pathConfig.getDataPath();
         FileUtils.deleteQuietly(dataPath.toFile());

         // Set this to remove any folders created by tests
         // when tearing down the class
         seaglePathConfig = pathConfig;
         initialized = true;

         eventManager.addListener(myListener);

         // Clone and check if everything was ok
         cloneAndCheck();
      }
   }

   private static int dirEntryNumber(Path dir) throws IOException {
      // Check that a directory was created within repositories dir
      // that would mean the clone was ok
      try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
         Iterator<Path> it = stream.iterator();
         int files = 0;
         while (it.hasNext()) {
            it.next();
            files++;
         }
         return files;
      }
   }

   @Test
   public void testExist() {
      assertTrue(projectManager.exists(purl1));
   }

   @Test
   public void testCheckOut() throws VCSRepositoryException, IOException {
      VCSRepository repo = null;
      try {
         
         repo = projectManager.getRepository(purl1);
         assertNotNull(repo);

         Collection<VCSBranch> branches = repo.getBranches();
         assertNotNull(branches);
         assertTrue(!branches.isEmpty());

         VCSBranch branch = branches.iterator().next();
         assertNotNull(branch);

         VCSCommit head = branch.getHead();
         assertNotNull(head);

         projectManager.checkout(repo, head);
         String folder = projectManager.getCheckoutFolder(repo, head);
         assertNotNull(folder);

         Path cd = Paths.get(folder);
         assertTrue(Files.isDirectory(cd));
         int entries = dirEntryNumber(cd);
         assertTrue(entries > 0);
      } finally {
         if (repo != null) {
            repo.close();
         }
      }

   }

   /**
    * Should be call before the very first test in order to test that the cloned
    * repo was successful.
    */
   private void cloneAndCheck() throws IOException {

      // Run clone in order to run with other tests
      projectManager.clone(purl1);

      VCSRepository repo = null;

      try {
         repo = projectManager.getRepository(purl1);
         assertNotNull(repo);

         Path path = Paths.get(repo.getLocalPath());

         assertTrue(Files.isDirectory(path));
         int n = dirEntryNumber(path);
         assertTrue(n > 0);
         assertEquals(purl1, repo.getRemotePath());

         // Check that this repository was saved in the desired location
         Path parent = seagleManager.getSeaglePathConfig().getRepositoriesPath();

         assertEquals(path.getParent().toAbsolutePath(), parent.toAbsolutePath());

         // Check the listener
         List<Event> events = myListener.getEventsOfType(ProjectEventType.CLONE);
         assertEquals(1, events.size());
         ProjectInfo info = (ProjectInfo) events.get(0).getInfo().getDescription();
         assertNotNull(info);
         assertEquals(Paths.get(repo.getLocalPath()).toAbsolutePath(),
                 Paths.get(info.getLocalFolder()).toAbsolutePath());
         assertEquals(repo.getRemotePath(), info.getRemoteUrl());
      } finally {
         if (repo != null) {
            repo.close();
         }
      }
   }
}
