package gr.uom.java.seagle.v2.project.build;

/**
 *
 * @author Daniel Feitosa
 */
public interface ProjectBuilder {
    
    /**
     * @return Name of the builder
     */
    public abstract String getName();
    
    /**
     * @return Version of the builder
     */
    public abstract String getVersion();
    
    /**
     * @return If the builder files are present (e.g., build.xml for maven)
     */
    public abstract boolean canBuild();
    
    /**
     * @return If the builder infrastructure is available
     */
    public abstract boolean isAvailable();
    
    /**
     * Try to build the project
     * @return If the project was successfully built
     */
    public abstract boolean tryToBuild();
    
    /**
     * @return Path to log file of the build (for debugging purposes).
     */
    public abstract String getPathToBuildLog();
    
    /**
     * @return Path to the main parent folder with class files.
     */
    public abstract String getPathClassFiles();
}
