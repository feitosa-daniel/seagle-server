/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;

/**
 * A Manager that handles operations relevant to local repository management.
 * <p>
 * Use this manager to gain access in the locally stored repositories and the
 * source code of the projects, as well as to download (clone) remote, update
 * and checkout repositories. Access to the instances of this interface should
 * be through seagle manager:
 * 
 * <pre>
 * RepositoryManager repoManager = seagleManager
 *       .getManager(RepositoryManager.class);
 * </pre>
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public interface RepositoryManager {

   /**
    * Clones the remote repository to a local folder and creates a local
    * repository.
    * <p>
    * 
    * @param remoteRepoPath
    *           The path of the remote repository of the project
    */
   void clone(String remoteRepoPath);

   /**
    * Updates the local repository to reflect newer (fresh) commits on the
    * remote repository.
    * <p>
    * 
    * @param remoteRepoPath
    *           The path of the remote repository of the project
    */
   void update(String remoteRepoPath);

   /**
    * Deletes the local repository of this project.
    * <p>
    * 
    * @param remoteRepoPath
    *           The path of the remote repository of the project
    */
   void delete(String remoteRepoPath);

   /**
    * Checks if the project with the given URI (remoteRepoPath) exist in the
    * local folder.
    * <p>
    * 
    * @param remoteRepoPath
    *           The path of the remote repository of the project
    * @return
    */
   boolean exists(String remoteRepoPath);

   /**
    * This VCSRepository is only for reading purposes. DO NOT USE THIS
    * REPOSITORY TO UPDATE OR CHECKOUT.
    *
    * @param repoPath
    *           the remote path of the repo
    * @return a VCSRepository of this path or null if a repository does not
    *         exist
    */
   VCSRepository getRepository(String repoPath);

   /**
    * Performs checkout of the commit that has the given commit ID.
    * <p>
    * 
    * @param commitID
    *           the ID of the commit to be checked out.
    * @param repoUrl
    *           the remote repo url
    */
   void checkout(String repoUrl, String commitID);

   /**
    * Return the folder where the checkout for the given remote repo URL and
    * given commit id is.
    * <p>
    * 
    * Although a checkout may have not been performed previously, this method is
    * a convenience method in order to resolve system internal paths, and
    * therefore should be used carefully.
    * <p>
    * Note that this method may fail if the system can not recognize the
    * remoteUrl and can not decide based on the url where the checkout folder
    * may be. This because the system would try to create a checkout folder
    * based on the repoUrl (usually by trying to extract the name of the project
    * from the url).
    * 
    * @param repoUrl
    *           the remote repo url
    * @param cid
    *           the comit id of the repository
    * @return the checkout folder, if the system has made a checkout for the
    *         given parameters.
    */
   String getCheckoutFolder(String repoUrl, String cid);

   /**
    * Return the folder where the system would save (clone from remote url) the
    * repository.
    * <p>
    * Although a repository may have not been cloned before for the given url,
    * and therefore the folder is empty, this method is a convenience method in
    * order to resolve system internal paths. The use of those paths should be
    * careful.
    * <p>
    * Note that this method may fail if the system can not recognize the
    * remoteUrl and can not decide based on the url where the repo folder may
    * be. This because the system would try to create a repo folder based on the
    * repoUrl (usually by trying to extract the name of the project from the
    * url).
    * 
    * @param repoUrl
    *           the origin of the repository from where it is (or will be)
    *           cloned.
    * @return the repository folder for the given remote url.
    */
   String getRepositoryFolder(String repoUrl);

   /**
    * Performs checkout of the commit that has the given commit ID.
    * <p>
    * Use this method in case the {@code repo} object is created using this
    * manager, otherwise this may cause later problems. To get a project given
    * its repo url use {@link #getRepository(java.lang.String) }.
    *
    * @param cid
    *           the ID of the commit to be checked out.
    * @param repo
    *           the repository object
    */
   void checkout(VCSRepository repo, String cid);

   /**
    * Return the folder where the checkout for the given repo and given commit
    * id is.
    * <p>
    *
    * @param repo
    *           the repo object created from this manager
    * @param cid
    *           the comit id of the repository
    * @return the checkout folder, if the system has made a checkout for the
    *         given parameters.
    */
   String getCheckoutFolder(VCSRepository repo, String cid);

   /**
    * Performs checkout of the commit that has the given commit ID.
    * <p>
    * Use this method in case the {@code repo} object is created using this
    * manager, otherwise this may cause later problems. To get a project given
    * its repo url use {@link #getRepository(java.lang.String) }.
    *
    * @param commit
    *           the ID of the commit to be checked out.
    * @param repo
    *           the repository object
    */
   void checkout(VCSRepository repo, VCSCommit commit);

   /**
    * Return the folder where the checkout for the given repo and given commit
    * is.
    * <p>
    *
    * @param repo
    *           the repo object created from this manager
    * @param commit
    *           the of the repository
    * @return the checkout folder, if the system has made a checkout for the
    *         given parameters.
    */
   String getCheckoutFolder(VCSRepository repo, VCSCommit commit);

   /**
    * Given the remote project url, the one that was used to clone the project
    * resolve a name for the project.
    * <p>
    * 
    * @param projectUrl
    *           the remote project url
    * @return a name for the given project url
    */
   String resolveProjectName(String projectUrl);
}
