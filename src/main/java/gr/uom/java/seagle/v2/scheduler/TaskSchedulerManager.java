package gr.uom.java.seagle.v2.scheduler;

import gr.uom.se.util.concurrent.TaskScheduler;
import gr.uom.se.util.concurrent.TaskType;
import java.util.Collection;

/**
 *
 * @author Elvis Ligu
 */
public interface TaskSchedulerManager extends TaskScheduler {
   
   TaskType getType(String id);
   
   Collection<TaskType> getTypes();
}
