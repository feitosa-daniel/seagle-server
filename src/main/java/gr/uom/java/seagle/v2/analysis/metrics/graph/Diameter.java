package gr.uom.java.seagle.v2.analysis.metrics.graph;

import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.graph.evolution.GraphEvolution;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.collections15.Transformer;

/**
 *
 * @author Theodore Chaikalis
 */
public class Diameter extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "GRAPH_DIAMETER";

    public Diameter(SeagleManager seagleManager) {
        super(seagleManager);
    }

    /**
     * Calculation of the Graph Diameter by using the average path distance This
     * is a better method than getting the max geodesic distance since it is
     * less prone to outliers.
     *
     * @param SoftwareProject - the project that contains the corresponding graph
     */
    @Override
    public void calculate(SoftwareProject softwareProject) {

        SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
        Transformer<AbstractNode, Double> avDists = DistanceStatistics.averageDistances(softwareGraph);
        Collection<Number> distances = new ArrayList<>();
        for (AbstractNode node : softwareGraph.getVertices()) {
            Double transform = avDists.transform(node);
            if (!transform.isNaN() && !transform.isInfinite() && !transform.equals((double) -1)) {
                distances.add(transform);
            }
        }
        double averageDistance = MetricAggregationStrategy.Average.getAggregatedValue(distances).doubleValue();
        softwareProject.putProjectLevelMetric(getMnemonic(), averageDistance);
    }

    @Override
    public String getMnemonic() {
        return "GRAPH_DIAMETER";
    }

    @Override
    public String getDescription() {
        return "Calculation of the Graph Diameter by using the average path distance\n"
                + "This is a better method than getting the max geodesic distance since it\n"
                + "is less prone to outliers.";
    }

    @Override
    public String getName() {
        return "Graph Diameter";
    }

}
