/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence;

import gr.uom.se.util.validation.ArgsCheck;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * An entity that links a metric to a project.
 * <p>
 * This is useful when we have metrics that are not related to versions but to a
 * project in general.
 * 
 * @author Elvis Ligu
 */
@Entity
@Table(name = "project_metric")
@IdClass(ProjectMetricPK.class)
@XmlRootElement
@NamedQueries({
   @NamedQuery(name = "ProjectMetric.findAll", query = "SELECT p FROM ProjectMetric p"),
   @NamedQuery(name = "ProjectMetric.findByMetricValueId", query = "SELECT p FROM ProjectMetric p WHERE p.metricValue = :metricValueId"),
   @NamedQuery(name = "ProjectMetric.findByMetric", query = "SELECT p FROM ProjectMetric p JOIN p.metricValue m WHERE m.metric = :metric"),
   @NamedQuery(name = "ProjectMetric.findByProjectAndMetric", query = "SELECT pm FROM ProjectMetric pm JOIN pm.metricValue m WHERE m.metric = :metric AND pm.project = :project"),
   @NamedQuery(name = "ProjectMetric.findByProjectId", query = "SELECT p FROM ProjectMetric p WHERE p.project = :projectId")})
public class ProjectMetric implements Serializable {

   private static final long serialVersionUID = 1333924927893533504L;

   
   /**
    * The project entity.
    * <p>
    */
   @Id
   @ManyToOne(optional = false)
   private Project project;
   
   /**
    * The metric value.
    * <p>
    */
   @Id
   @OneToOne(cascade = CascadeType.ALL, optional = false)
   private MetricValue metricValue;
   
   /**
    * 
    */
   public ProjectMetric() {
   }
   
   public ProjectMetric(Project project, MetricValue value) {
      ArgsCheck.notNull("project", project);
      ArgsCheck.notNull("value", value);
      this.project = project;
      this.metricValue = value;
   }

   public Project getProject() {
      return project;
   }

   public void setProject(Project project) {
      this.project = project;
   }

   public MetricValue getMetricValue() {
      return metricValue;
   }

   public void setMetricValue(MetricValue metricValue) {
      this.metricValue = metricValue;
   }

   @Override
   public int hashCode() {
      int hash = 7;
      hash = 19 * hash + metricValue.hashCode() + project.hashCode();
      return hash;
   }  
}
