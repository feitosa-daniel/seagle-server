package gr.uom.java.seagle.v2.project.build;

import gr.uom.java.seagle.v2.SeagleManager;

/**
 * 
 * @author Daniel Feitosa
 */
public class BuilderFactory {
    
    /**
     * Create Create a builder 
     * @param folder
     * @return 
     */
    public static ProjectBuilder createBuilder(SeagleManager seagleManager, String folder) throws UnknownBuilderException {
        if(MavenBuilder.canBuild(folder))
            return new MavenBuilder(seagleManager, folder);
        else 
        if(AntBuilder.canBuild(folder))
            return new AntBuilder(seagleManager, folder);
        else 
        if(GradleBuilder.canBuild(folder))
            return new GradleBuilder(seagleManager, folder);
        else
            throw new UnknownBuilderException(folder);
    }
}
