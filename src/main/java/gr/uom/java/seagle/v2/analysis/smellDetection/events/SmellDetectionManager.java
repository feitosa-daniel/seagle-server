package gr.uom.java.seagle.v2.analysis.smellDetection.events;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.AnalysisRequestInfo;
import gr.uom.java.seagle.v2.analysis.smellDetection.AbstractSmellDetector;
import gr.uom.java.seagle.v2.analysis.smellDetection.SmellManager;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.controllers.BadSmellFacade;
import gr.uom.java.seagle.v2.db.persistence.controllers.NodeFacade;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.project.ProjectManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Logger;

/**
 * 
 *
 * @author Thodoris Chaikalis
 */
public class SmellDetectionManager {

    private final EventManager eventManager;
    private final BadSmellFacade badSmellFacade;
    private final NodeFacade nodeFacade;
    private final Project project;

    private static final Logger logger = Logger.getLogger(SmellDetectionManager.class.getName());
    
    private final Collection<Node> nodesToUpdate;

    public SmellDetectionManager(SeagleManager seagleManager, AnalysisRequestInfo ainfo) {
        this.badSmellFacade = seagleManager.resolveComponent(BadSmellFacade.class);
        this.nodeFacade = seagleManager.resolveComponent(NodeFacade.class);
        this.eventManager = seagleManager.resolveComponent(EventManager.class);
        SmellManager.createSmells(badSmellFacade, seagleManager);
        ProjectManager projectManager = seagleManager.resolveComponent(ProjectManager.class);
        this.project = projectManager.findByUrl(ainfo.getRemoteProjectUrl());
        this.nodesToUpdate = Collections.synchronizedCollection(new HashSet<Node>());
    }

    public void compute() {
        logger.info("Starting identification of code smells for project "+project.getName());
         for (AbstractSmellDetector m : SmellManager.getDetectionStrategies()) {
            if (m instanceof AbstractSmellDetector) {
                AbstractSmellDetector smellDetector = (AbstractSmellDetector) m;
                smellDetector.detectProjectSmells(nodesToUpdate, project);
            }
        }
        updateDB();
        logger.info("Identification of code smells Completed for "+project.getName());
    }

    protected void updateDB() {
        nodeFacade.edit(new ArrayList<Node>(nodesToUpdate));
        triggerSmellDetectionEnded();
    }

    private void triggerSmellDetectionEnded() {
        EventInfo eInfo = new EventInfo(project, new Date());
        Event smellDetectENDED = SmellDetectionEventType.SMELL_DETECTION_ENDED.newEvent(eInfo);
        eventManager.trigger(smellDetectENDED);
    }

}
