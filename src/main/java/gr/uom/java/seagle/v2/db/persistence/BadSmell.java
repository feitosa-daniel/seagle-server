package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Thodoris Chaikalis
 */
@Entity
@Table(name = "BadSmell")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BadSmell.findAll", query = "SELECT bs FROM BadSmell bs"),
    @NamedQuery(name = "BadSmell.findById", query = "SELECT bs FROM BadSmell bs WHERE bs.id = :id"),
    @NamedQuery(name = "BadSmell.findByName", query = "SELECT bs FROM BadSmell bs WHERE bs.name = :name")})
public class BadSmell implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1024)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = true)
    @NotNull
    @Lob
    @Column(name = "description")
    private String description;

    public BadSmell(){}

    public BadSmell(String name, String description) {
        this.name = name;
        this.description = description;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
       
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    

    @Override
    public int hashCode() {
        int hash = 17;
        hash += 34 + (id != null ? id.hashCode() : 0);
        hash += 34 + (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BadSmell)) {
            return false;
        }
        BadSmell other = (BadSmell) object;
        if(Objects.equals(this.id, other.id)){
            return true;
        }
        if (this.name.equals(other.name)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "gr.uom.java.seagle.v2.db.persistence.BadSmell[ id=" + id + " ]";
    }

}
