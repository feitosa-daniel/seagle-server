package gr.uom.java.seagle.v2.analysis.metrics.graph;

import edu.uci.ics.jung.algorithms.metrics.Metrics;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.graph.AbstractEdge;
import gr.uom.java.seagle.v2.analysis.graph.AbstractNode;
import gr.uom.java.seagle.v2.analysis.graph.SoftwareGraph;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.SoftwareProject;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Theodore Chaikalis
 */
public class ClusteringCoefficient extends AbstractGraphExecutableMetric {

    public static final String MNEMONIC = "CLUSTERING_COEFFICIENT";

    public ClusteringCoefficient(SeagleManager manager) {
        super(manager);
    }

    @Override
    public void calculate(SoftwareProject softwareProject) {
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        SoftwareGraph<AbstractNode, AbstractEdge> softwareGraph = softwareProject.getProjectGraph();
        Map<AbstractNode, Double> clusteringCoefficients = Metrics.clusteringCoefficients(softwareGraph);
        for (AbstractNode node : softwareGraph.getVertices()) {
            double nodeBtwnCentr = clusteringCoefficients.get(node);
            valuePerClass.put(node.getName(), nodeBtwnCentr);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, softwareProject);
        storeProjectLevelAggregationMetricInMemory(getMnemonic(), softwareProject, valuePerClass, MetricAggregationStrategy.Average);
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Calculates Clustering Coefficient of each graph node and the average of the whole graph";
    }

    @Override
    public String getName() {
        return "Clustering Coefficient";
    }

}
