/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.se.util.module.ModuleConstants;
import gr.uom.se.util.module.ModuleContext;
import gr.uom.se.util.property.DomainPropertyHandler;
import gr.uom.se.util.property.PropertyHandler;
import gr.uom.se.vcs.VCSRepository;

import java.io.Closeable;

/**
 * A project context that is able to provide specific resources of the project.
 * <p>
 * A project is related to a repository from where the clients can read project
 * artifacts (expected functionality for other susbsystems such as bug tracking
 * system). A repository can be analyzed in order to gather useful information.
 * Some utilities that can be used to analyze a repository are the version
 * providers. Using a project context a client may be provided a version
 * provider for the given project (repository). The client can create its
 * modules dynamically using the context and expect its modules to be injected
 * project resources. Suppose the client has access to an interface of a module
 * called RepoAnalyzer. Of course the client has no access to the implementation
 * of that module. In order for the client to create an instance of this module
 * he can work as follows:
 * 
 * <pre>
 * // Get an instance of context manager to lookup the context
 * ContextManager cm = seagleManager.getManager(ContextManager.class);
 * 
 * // Lookup the project context
 * try (ProjectContext context = cm.lookupContext(myProject, ProjectContext.class)) {
 * 
 *    // Create the repository analyzer
 *    RepoAnalyzer analyzer = context.load(RepoAnalyzer.class);
 *    // Do the work here
 *    analyzer.analyzeAll();
 * }
 * </pre>
 * 
 * The repository analyzer implementation may have a dependency on
 * ConnectedVersionProvider or VersionNameResolver or the repository and project
 * itself. The implementation should adhere to modules API (if used the
 * constructor of the implementation - see modules API - then it should have all
 * dependencies listed as parameters). If a specific implementation provider is
 * used then it depends on how the provider works. However for any dependency
 * from project context the client should list it in the provider
 * method/constructor (depending on how it is provided). Having this in mind
 * clients may create objects that have dependencies for project contexts so
 * they can use project specific resource. However they can provide to third
 * parts only the interfaces of their objects, and require the use of project
 * context in order to load these objects.
 * <p>
 * Note that project context should be closed after its usage. This will ensure
 * that any open resource be released (especially the repository resources).
 * also a project context should be considered a one shot instance and should
 * not be shared by two different threads.
 * 
 * @author Elvis Ligu
 */
public interface ProjectContext extends ModuleContext, DomainPropertyHandler,
      PropertyHandler, Closeable {

   /**
    * Get the repository this context is related to.
    * <p>
    * The clients should close the repository (or the context), after they
    * finish their job.
    * 
    * @return the repository related to this context.
    */
   VCSRepository getRepository();

   /**
    * Get the project this context is related to.
    * <p>
    * 
    * @return the project this context is related to.
    */
   Project getProject();

   /**
    * {@inheritDoc}
    * <p>
    * The property specified using this method will be stored under
    * {@link ModuleConstants#DEFAULT_PROPERTY_ANNOTATION_DOMAIN} domain.
    */
   @Override
   public void setProperty(String name, Object value);

   /**
    * {@inheritDoc}
    * <p>
    * The property will be read from
    * {@link ModuleConstants#DEFAULT_PROPERTY_ANNOTATION_DOMAIN} domain.
    */
   @Override
   public Object getProperty(String name);

   /**
    * {@inheritDoc}
    * <p>
    * The property will be read from
    * {@link ModuleConstants#DEFAULT_PROPERTY_ANNOTATION_DOMAIN} domain.
    */
   @Override
   public <T> T getProperty(String name, Class<T> type);
}
