/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.metrics.aggregation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author Thodoris
 */
public enum MetricAggregationStrategy {

    Average {
                @Override
                public Number getAggregatedValue(Collection<? extends Number> numbers) {
                    double average = 0;
                    for (Number num : numbers) {
                        average += num.doubleValue();
                    }
                    if (!numbers.isEmpty()) {
                        average = average / numbers.size();
                    }
                    return average;
                }

            },
    Max {
                @Override
                public Number getAggregatedValue(Collection<? extends Number> numbers) {
                    Collection<Double> numbersD = new ArrayList<Double>();
                    for (Number num : numbers) {
                        numbersD.add(num.doubleValue());
                    }
                    return Collections.max(numbersD);

                }

            },
    Min {

                @Override
                public Number getAggregatedValue(Collection<? extends Number> numbers) {
                    Collection<Double> numbersD = new ArrayList<Double>();
                    for (Number num : numbers) {
                        numbersD.add(num.doubleValue());
                    }
                    return Collections.min(numbersD);
                }

            },
    Sum {

                @Override
                public Number getAggregatedValue(Collection<? extends Number> numbers) {
                    double sum = 0;
                    for (Number num : numbers) {
                        sum += num.doubleValue();
                    }
                    return sum;
                }

            };

    public abstract Number getAggregatedValue(Collection<? extends Number> numbers);
}
