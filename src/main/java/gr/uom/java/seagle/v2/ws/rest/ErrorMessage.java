/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest;

import java.net.URI;

import javax.ws.rs.NotFoundException;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "error")
public class ErrorMessage {
   /** contains the same HTTP Status code returned by the server */
   @XmlElement(name = "status")
   int status;
   
   /** application specific error code */
   @XmlElement(name = "code")
   int code;
   
   /** message describing the error*/
   @XmlElement(name = "message")
   String message;
      
   /** link point to page where the error message is documented */
   @XmlElement(name = "link")
   String link;
   
   /** extra information that might useful for developers */
   @XmlElement(name = "developerMessage")
   String developerMessage;   

   public int getStatus() {
      return status;
   }

   public void setStatus(int status) {
      this.status = status;
   }

   public int getCode() {
      return code;
   }

   public void setCode(int code) {
      this.code = code;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public String getDeveloperMessage() {
      return developerMessage;
   }

   public void setDeveloperMessage(String developerMessage) {
      this.developerMessage = developerMessage;
   }

   public String getLink() {
      return link;
   }

   public void setLink(String link) {
      this.link = link;
   }
   
   public ErrorMessage(RestApiException ex){
      this.code = ex.code;
      this.status = ex.status;
      this.developerMessage = ex.developerMessage;
      this.link = ex.link;
      this.message = ex.getMessage();
   }
   
   public ErrorMessage(NotFoundException ex){
      this.status = ex.getResponse().getStatus();
      this.message = ex.getMessage();
      URI uri = ex.getResponse().getLocation();
      String link = uri != null ? uri.toString() : RestApiConstants.SEAGLE_UOM_URL;
      this.link = link;
   }

   public ErrorMessage() {}
}
