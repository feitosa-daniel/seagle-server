package gr.uom.java.seagle.v2.analysis.smellDetection;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOAM;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.NOPA;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.WMC;
import gr.uom.java.seagle.v2.analysis.metrics.sourceCode.WOC;
import gr.uom.java.seagle.v2.db.persistence.BadSmell;
import gr.uom.java.seagle.v2.db.persistence.Node;
import gr.uom.java.seagle.v2.db.persistence.Project;
import gr.uom.java.seagle.v2.db.persistence.Version;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Thodoris Chaikalis version 0.1 20/10/15
 */
public class DataClassDetector extends AbstractSmellDetector {

    public static final String MNEMONIC = "DATA_CLASS_DETECTOR";
    private static final Logger logger = Logger.getLogger(DataClassDetector.class.getName());

    public DataClassDetector(SeagleManager seagleManager) {
        super(seagleManager, MNEMONIC);
    }

    @Override
    public void detectSmells(Project project) {
        BadSmell dataClassBadSmell = SmellManager.getSmell(BadSmellEnum.DATA_CLASS.getMnemonic());

        for (Version version : project.getVersions()) {
            logger.log(Level.INFO, "Detecting Data Class Smell for version : {0}", version.getName());
            Map<String, Double> perClassWMC = getMetricValuesPerClass(version, WMC.MNEMONIC);
            Map<String, Double> perClassWOC = getMetricValuesPerClass(version, WOC.MNEMONIC);
            Map<String, Double> perClassNOPA = getMetricValuesPerClass(version, NOPA.MNEMONIC);
            Map<String, Double> perClassNOAM = getMetricValuesPerClass(version, NOAM.MNEMONIC);

            Collection<Node> vertices = nodeFacade.findByVersion(version);

            for (Node dbNode : vertices) {
                if (!dbNode.hasSmell(dataClassBadSmell)) {
                     try{
                        String nodeName = dbNode.getName();
                        double woc = perClassWOC.get(nodeName);
                        double nopa = perClassNOPA.get(nodeName);
                        double noam = perClassNOAM.get(nodeName);
                        double wmc = perClassWMC.get(nodeName);

                        double wmcHIGH = getHIGHlimitOfMetric(perClassWMC, WMC.MNEMONIC);
                        double wmcVERY_HIGH = wmcHIGH * 1.5;

                        boolean wocLessThanOneThird = (woc < (1.0 / 3.0));
                        boolean dataExposureAndModerateComplexity = (((nopa + noam) > few) && (wmc < wmcHIGH));
                        boolean increasedDataExposureAndComplexity = (((nopa + noam) > many) && (wmc < wmcVERY_HIGH));

                        if (wocLessThanOneThird && (dataExposureAndModerateComplexity || increasedDataExposureAndComplexity)) {
                            dbNode.addBadSmell(dataClassBadSmell);
                            nodesToUpdate.add(dbNode);
                            logger.log(Level.INFO, "Data class smell found for project version {0} class: {1}", new Object[]{version.getName(), dbNode.getName()});
                        }
                    }catch(Exception e){
                            logger.log(Level.INFO, "exception trhown while detecting Data class smell for node {0} version: {1}", new Object[]{dbNode.getName(), version.getName()});
                        }
                }
            }
        }
    }

}
