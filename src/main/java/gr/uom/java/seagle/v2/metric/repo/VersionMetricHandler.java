/**
 * 
 */
package gr.uom.java.seagle.v2.metric.repo;

import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.metric.MetricRunInfo;
import gr.uom.java.seagle.v2.metric.imp.AbstractMetricHandler;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * @author Elvis Ligu
 */
public class VersionMetricHandler extends AbstractMetricHandler {

   /**
    * @param seagleManager
    */
   public VersionMetricHandler(
         @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager) {
      super(seagleManager);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public Runnable getRunner(List<ExecutableMetric> metrics, MetricRunInfo info) {
      return new VersionMetricsTask(seagleManager, info,
            new HashSet<ExecutableMetric>(metrics));
   }

   /**
    * {@inheritDoc}
    */
   @Override
   protected Collection<ExecutableMetric> computeMetrics(MetricRunInfo info,
         List<ExecutableMetric> metrics) {

      // Check for each metric its metadata (DB operation) and retain only
      // those metrics that needs to be updated
      List<ExecutableMetric> myMetrics = new ArrayList<>(metrics.size());
      for(int i = 0; i < metrics.size(); i++) {
         myMetrics.add(metrics.get(i));
      }
      
      myMetrics.retainAll(CommitProcessorFactory.SUPPORTED_METRICS);
      Collection<String> retain = extractMnemonics(myMetrics);
      MetricMetadataChecker checker = MetricMetadataChecker.getInstance();
      String purl = getRequest(info).getRemoteProjectUrl();

      retain = checker.retainForNewBranchesUpdate(seagleManager, purl, retain);
      retain = checker.retainForNewVersionsUpdate(seagleManager, purl, retain);

      return retainMetrics(myMetrics, retain);
   }

   private Collection<String> extractMnemonics(List<ExecutableMetric> metrics) {
      Collection<String> mnemonics = new ArrayList<>(metrics.size());
      for (ExecutableMetric em : metrics) {
         mnemonics.add(em.getMnemonic());
      }
      return mnemonics;
   }

   private Collection<ExecutableMetric> retainMetrics(
         Collection<ExecutableMetric> metrics, Collection<String> mnemonics) {

      Collection<ExecutableMetric> retain = new ArrayList<ExecutableMetric>();
      for (ExecutableMetric em : metrics) {
         if (mnemonics.contains(em.getMnemonic())) {
            retain.add(em);
         }
      }
      return retain;
   }
}
