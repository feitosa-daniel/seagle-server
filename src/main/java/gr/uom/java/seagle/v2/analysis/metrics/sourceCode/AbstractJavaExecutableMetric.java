package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.AbstractAnalysisMetric;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import gr.uom.java.seagle.v2.metric.Category;
import gr.uom.java.seagle.v2.metric.Language;
import gr.uom.java.seagle.v2.metric.imp.ExecutableMetric;

/**
 *
 * @author Thodoris Chaikalis ver. 0.0.1 - 11/9/2015
 * ver. 0.0.2 - 6/12/15
 */
public abstract class AbstractJavaExecutableMetric extends AbstractAnalysisMetric implements ExecutableMetric {

    public AbstractJavaExecutableMetric(SeagleManager seagleManager) {
        super(seagleManager);
    }

    public abstract void calculate(SystemObject systemObject, JavaProject javaProject);

    @Override
    public String getCategory() {
        return Category.SRC_METRICS;
    }

    @Override
    public String[] getLanguages() {
        return new String[]{Language.JAVA};
    }

}
