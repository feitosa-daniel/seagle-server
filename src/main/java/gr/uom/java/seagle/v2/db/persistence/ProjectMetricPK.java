/**
 * 
 */
package gr.uom.java.seagle.v2.db.persistence;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 * The primary key of ProjectMetric.
 * <p>
 * 
 * @author Elvis Ligu
 */
@Embeddable
public class ProjectMetricPK implements Serializable {

   private static final long serialVersionUID = -8759813553567452544L;

   private Long project;
   
   private Long metricValue;

   /**
    * 
    */
   public ProjectMetricPK() {
   }

   public Long getProject() {
      return project;
   }

   public Long getMetricValue() {
      return metricValue;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + project.hashCode();
      result = prime * result + metricValue.hashCode();
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      ProjectMetricPK other = (ProjectMetricPK) obj;
      if (!project.equals(other.project))
         return false;
      if (!metricValue.equals(other.metricValue))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "ProjectMetricPK [projectId=" + project + ", valueId=" + metricValue+ "]";
   }
}
