/**
 * 
 */
package gr.uom.java.seagle.v2.project;

import gr.uom.java.seagle.v2.SeagleConstants;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.SeaglePathConfig;
import gr.uom.java.seagle.v2.event.EventManager;
import gr.uom.java.seagle.v2.policy.FilePolicy;
import gr.uom.java.seagle.v2.project.nameResolver.RepositoryInfoResolverFactory;
import gr.uom.se.util.config.ConfigManager;
import gr.uom.se.util.event.Event;
import gr.uom.se.util.event.EventInfo;
import gr.uom.se.util.module.annotations.NULLVal;
import gr.uom.se.util.module.annotations.Property;
import gr.uom.se.util.module.annotations.ProvideModule;
import gr.uom.se.util.validation.ArgsCheck;
import gr.uom.se.vcs.VCSBranch;
import gr.uom.se.vcs.VCSCommit;
import gr.uom.se.vcs.VCSRepository;
import gr.uom.se.vcs.exceptions.VCSRepositoryException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.io.FileUtils;

/**
 * A default implementation of {@link RepositoryManager}.
 * <p>
 * This class is based on managers API and modules API thus it should be
 * included in configuration files info to register this manager class when
 * seagle is starting and to load this object when it is requested.
 * 
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class DefaultRepositoryManager implements RepositoryManager {

   /**
    * The seagle manager injected in order to get access to configs.
    * <p>
    */
   private final SeagleManager seagleManager;

   private final EventManager eventManager;

   /**
    * This should be injected at constructor time.
    * 
    */
   private final RepositoryProvider repoProvider;

   @ProvideModule
   public DefaultRepositoryManager(
         @Property(name = NULLVal.NO_PROP) SeagleManager seagleManager,
         @Property(name = NULLVal.NO_PROP) EventManager eventManager,
         @Property(name = NULLVal.NO_PROP) RepositoryProvider repoProvider) {

      ArgsCheck.notNull("seagleManager", seagleManager);
      ArgsCheck.notNull("eventManager", eventManager);
      ArgsCheck.notNull("repository provider", repoProvider);
      this.seagleManager = seagleManager;
      this.eventManager = eventManager;
      this.repoProvider = repoProvider;
   }

   /**
    * {@inheritDoc}
    * <p>
    * The local repo path will be locked while this method is executing. A
    * {@link ProjectEventType#CLONE} event will be sent to event manager.
    */
   @Override
   public void clone(String remoteRepoPath) {
      ArgsCheck.notEmpty("remoteRepoPath", remoteRepoPath);

      // Get the file policy in order to lock the folders
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the location were to store this new repo
      Path localRepoPath = resolveRepoPath(remoteRepoPath);

      filePolicy.writeLockPath(localRepoPath);

      try {
         // Check first that the repo is not already there
         // if so no clone can be perform
         // WRANING: if a folder with the same name as
         // the folder of repo is present and have some
         // contents it will not know if it is a repo or not
         if (!Files.isDirectory(localRepoPath)) {
            try (VCSRepository repo = repoProvider.getRepositoryForRemoteUrl(remoteRepoPath)) {

               // Clone repo
               repo.cloneRemote();

               // Trigger a CLONE event
               eventManager.trigger(createEvent(ProjectEventType.CLONE,
                     remoteRepoPath, localRepoPath.toString()));
            }
         }
      } catch (VCSRepositoryException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(localRepoPath);
      }
   }

   /**
    * Create a new event to push it through event manager.
    * <p>
    * 
    * @param type
    * @param repoUrl
    * @param localUrl
    * @return
    */
   private Event createEvent(ProjectEventType type, String repoUrl,
         String localUrl) {
      String pName = extractProjectName(repoUrl);
      ProjectInfo pInfo = new ProjectInfo(pName, repoUrl, localUrl);
      EventInfo eInfo = new EventInfo(pInfo, new Date());
      return type.newEvent(eInfo);
   }

   /**
    * {@inheritDoc}
    * <p>
    * The local repo path will be locked while this method is executing. A
    * {@link ProjectEventType#UPDATE} event will be sent to event manager.
    */
   @Override
   public void update(String remoteRepoPath) {
      ArgsCheck.notEmpty("remoteRepoPath", remoteRepoPath);

      // Get the file policy to lock down the folders
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the path of the repo given its url
      Path repoPath = resolveRepoPath(remoteRepoPath);

      filePolicy.writeLockPath(repoPath);
      try {
         // We should check if there is a folder for the given repo,
         // if so we assume there is a repo
         if (Files.isDirectory(repoPath)) {
            try (VCSRepository repo = repoProvider.getRepositoryForRemoteUrl(remoteRepoPath)) {
               // Update repo
               repo.update();

               // Trigger an UPDATE event
               eventManager.trigger(createEvent(ProjectEventType.UPDATE,
                     remoteRepoPath, repoPath.toString()));
            }

         }
      } catch (VCSRepositoryException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(repoPath);
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * The local repo path and source code path will be locked while this method
    * is executing. A {@link ProjectEventType#REMOVE} event will be sent to
    * event manager.
    */
   @Override
   public void delete(String remoteRepoPath) {
      // Get the file policy to lock down folders
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the path of the repo
      Path repoPath = resolveRepoPath(remoteRepoPath);
      Path sourcePath = resolveProjectSourceCodePath(remoteRepoPath);

      filePolicy.writeLockPath(repoPath);
      filePolicy.writeLockPath(sourcePath);
      try {
         // If the path exists and is a directory
         // delete it
         if (Files.isDirectory(repoPath)) {
            try {
               FileUtils.deleteDirectory(repoPath.toFile());
            } finally {
               // Delete project source codes to
               if (Files.isDirectory(sourcePath)) {
                  FileUtils.deleteDirectory(sourcePath.toFile());
               }
            }

            // Trigger a DELETE event
            eventManager.trigger(createEvent(ProjectEventType.REMOVE,
                  remoteRepoPath, repoPath.toString()));
         }

      } catch (IOException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(sourcePath);
         filePolicy.writeUnlockPath(repoPath);
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * Will try to read a repo for the given repository. If the repo can be
    * loaded and have at least a branch it will be considered as it exists. The
    * local repo path will be read locked while this method is executing.
    */
   @Override
   public boolean exists(String remoteRepoPath) {
      // Get the file policy to lock down the folders
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the local repo path
      Path repoPath = resolveRepoPath(remoteRepoPath);

      filePolicy.readLockPath(repoPath);
      try {

         // If there is no directory there is no
         // need to check for repository
         if (!Files.isDirectory(repoPath)) {
            return false;
         }

         // Try to load a repository object and
         // read branches
         boolean exists = false;
         try (VCSRepository repo = repoProvider
               .getRepositoryForRemoteUrl(remoteRepoPath)) {

            Collection<VCSBranch> branches = repo.getBranches();
            if (!branches.isEmpty()) {
               exists = true;
            }
         } catch (VCSRepositoryException ex) {
            // Usually there will be an exception here if the repository
            // can not be read so we dismiss it
         }
         return exists;
      } finally {
         filePolicy.readUnlockPath(repoPath);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public VCSRepository getRepository(String remoteRepoPath) {
      // Get the file policy to lock down the folders
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the local repo path
      Path repoPath = resolveRepoPath(remoteRepoPath);
      filePolicy.readLockPath(repoPath);
      try {
         // If the file directory then create a repo object based on this
         // location and return it
         if (Files.isDirectory(repoPath)) {
            return repoProvider
                  .getRepositoryForRemoteUrl(remoteRepoPath);
         }
         return null;
      } finally {
         filePolicy.readUnlockPath(repoPath);
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * A repository object will be loaded and it will try to resolve the commit
    * with the given ID, if it can not be resolved it will throw an exception.
    * The repository local path and source path will be locked while this method
    * is executing.
    */
   @Override
   public void checkout(String repoUrl, String commitID) {
      // Get the file policy to lock the folder
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the folder of source codes
      Path sourcePath = resolveProjectSourceCodePath(repoUrl);
      // Get the folder of repository for the given url
      Path repoPath = resolveRepoPath(repoUrl);

      // We should lock now with write lock the source path
      // with write lock the checkout path
      // and with read lock the repository path (we will be only reading from)
      // First of is to lock the repo with reading so we are sure
      // no one can update/delete the repo
      filePolicy.readLockPath(repoPath);
      // The whole source path will be locked with write
      // so no other can write or read until the checkout is
      // performed (no need to lock the checkout path)
      filePolicy.writeLockPath(sourcePath);
      try {
         // If the repo doesn't exist we should throw an exception
         // because we can not create the repo object
         if (!Files.isDirectory(repoPath)) {
            throw new RuntimeException("repo doesn't exist for " + repoUrl);
         }
         // Get the folder where to checkout out the given commit
         // for the given repository
         Path checkoutPath = resolveCheckoutFolder(sourcePath, commitID);

         // We should check now if the folder of source codes exist
         // if not we should create it for the first time
         // This will ensure that the folder of source code
         // will be present
         Files.createDirectories(sourcePath);

         // Now we should check if the checkout folder
         // is not present. If its not present we should
         // make the checkout.
         if (!Files.isDirectory(checkoutPath)) {
            try (VCSRepository repo = repoProvider.getRepositoryForRemoteUrl(repoUrl)) {
               // Resolve the commit
               VCSCommit commit = repo.resolveCommit(commitID);

               // Call the checkout method
               checkout(commit, checkoutPath);
            }
         }
      } catch (VCSRepositoryException | IOException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(sourcePath);
         filePolicy.readUnlockPath(repoPath);
      }
   }

   /**
    * Checkout with rolling back if any exception occurs.
    * <p>
    *
    * @param commit
    * @param checkoutPath
    */
   private void checkout(VCSCommit commit, Path checkoutPath) {
      // Now we should check if the checkout folder
      // is not present. If its not present we should
      // make the checkout.
      if (!Files.isDirectory(checkoutPath)) {
         try {
            // The commit is resolved so we should create the checkout folder
            Files.createDirectories(checkoutPath);
            commit.checkout(checkoutPath.toAbsolutePath().toString());
         } catch (Exception ex) {
            // Rollback if any problem occurs
            FileUtils.deleteQuietly(checkoutPath.toFile());
            throw new RuntimeException(ex);
         }
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * The repository local path and source path will be locked while this method
    * is executing.
    */
   @Override
   public void checkout(VCSRepository repo, VCSCommit commit) {
      // Get the file policy to lock the folder
      FilePolicy filePolicy = resolveFilePolicy();
      // Get the folder of source codes
      Path sourcePath = resolveProjectSourceCodePath(repo.getRemotePath());
      // Get the folder of repository for the given url
      Path repoPath = Paths.get(repo.getLocalPath());

      // We should lock now with write lock the source path
      // with write lock the checkout path
      // and with read lock the repository path (we will be only reading from)
      // First of is to lock the repo with reading so we are sure
      // no one can update/delete the repo
      filePolicy.readLockPath(repoPath);
      // The whole source path will be locked with write
      // so no other can write or read until the checkout is
      // performed (no need to lock the checkout path)
      filePolicy.writeLockPath(sourcePath);
      try {

         // Get the folder where to checkout out the given commit
         // for the given repository
         Path checkoutPath = resolveCheckoutFolder(sourcePath, commit.getID());

         // We should check now if the folder of source codes exist
         // if not we should create it for the first time
         // This will ensure that the folder of source code
         // will be present
         Files.createDirectories(sourcePath);

         // Call the checkout method
         checkout(commit, checkoutPath);

      } catch (IOException ex) {
         throw new RuntimeException(ex);
      } finally {
         filePolicy.writeUnlockPath(sourcePath);
         filePolicy.readUnlockPath(repoPath);
      }
   }

   /**
    * {@inheritDoc}
    * <p>
    * A repository object will be loaded and it will try to resolve the commit
    * with the given ID, if it can not be resolved it will throw an exception.
    * The repository local path and source path will be locked while this method
    * is executing. This is the same as calling
    * {@link #checkout(VCSRepository, VCSCommit)} after the resolving of commit
    * object.
    */
   @Override
   public void checkout(VCSRepository repo, String cid) {
      ArgsCheck.notNull("repo", repo);
      ArgsCheck.notEmpty("cid", cid);
      try {
         VCSCommit commit = repo.resolveCommit(cid);
         checkout(repo, commit);
      } catch (VCSRepositoryException ex) {
         throw new RuntimeException(ex);
      }
   }

   /**
    * Get the file policy from seagle manager or throw an exception if not
    * found.
    */
   protected FilePolicy resolveFilePolicy() {
      FilePolicy filePolicy = seagleManager.getManager(FilePolicy.class);
      if (filePolicy == null) {
         throw new RuntimeException("Unable to resolve File Policy manager");
      }
      return filePolicy;
   }

   /**
    * Get the repository path for given a remote repo ur.
    */
   private Path resolveRepoPath(String repoUrl) {
      ArgsCheck.notEmpty("repoUrl", repoUrl);

      SeaglePathConfig pathConfig = resolvePathConfig();
      String pName = extractProjectName(repoUrl);

      Path reposPath = pathConfig.getRepositoriesPath();
      return reposPath.resolve(pName);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getRepositoryFolder(String repoUrl) {
      return resolveRepoPath(repoUrl).toAbsolutePath().toString();
   }

   /**
    * Get the checkout out folder.
    */
   private Path resolveCheckoutFolder(Path sourceFolder, String cid) {
      return sourceFolder.resolve(cid);
   }

   /**
    * Get the location where checkouts should be stored.
    */
   private Path resolveProjectSourceCodePath(String repoUrl) {
      ArgsCheck.notEmpty("repoUrl", repoUrl);

      SeaglePathConfig pathConfig = resolvePathConfig();
      String pName = extractProjectName(repoUrl);

      Path projectSourceCodePath = pathConfig.getProjectSourceCodePath();
      return projectSourceCodePath.resolve(pName);
   }

   /**
    * Given a url get a project name.
    */
   private String extractProjectName(String repoUrl) {
      String pName = RepositoryInfoResolverFactory.getInstance()
            .tryResolveName(repoUrl);
      if (pName == null) {
         throw new RuntimeException(
               "Unable to extract a legal project name from the given repo URL");
      }
      return pName;
   }

   /**
    * Get the seagle path config object.
    */
   private SeaglePathConfig resolvePathConfig() throws RuntimeException {
      ConfigManager config = seagleManager.getManager(ConfigManager.class);
      SeaglePathConfig pathConfig = config.getProperty(
            SeagleConstants.SEAGLE_DOMAIN, SeaglePathConfig.PROPERTY,
            SeaglePathConfig.class);
      if (pathConfig == null) {
         throw new RuntimeException(
               "Unable to resolve SeaglePathConfig instance");
      }
      return pathConfig;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(String repoUrl, String cid) {
      ArgsCheck.notEmpty("repoUrl", repoUrl);
      ArgsCheck.notEmpty("cid", cid);

      Path projectSourceCodePath = resolveProjectSourceCodePath(repoUrl);
      return resolveCheckoutFolder(projectSourceCodePath, cid).toString();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(VCSRepository repo, String cid) {
      ArgsCheck.notNull("repoUrl", repo);
      return getCheckoutFolder(repo.getRemotePath(), cid);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getCheckoutFolder(VCSRepository repo, VCSCommit commit) {
      ArgsCheck.notNull("commit", commit);
      return getCheckoutFolder(repo, commit.getID());
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String resolveProjectName(String projectUrl) {
      return extractProjectName(projectUrl);
   }
}
