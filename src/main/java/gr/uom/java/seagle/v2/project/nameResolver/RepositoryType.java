/**
 * 
 */
package gr.uom.java.seagle.v2.project.nameResolver;

/**
 * A marker interface to mark a repository type.
 * <p>
 * Generally speaking a repository type instance should be a static instance
 * that is used in all places, and returns true in equals methods. A good
 * candidate to achieve this is an Enumeration that implements this interface
 * and there should be only one instance per enumeration entry.
 * 
 * @author Elvis Ligu
 */
public interface RepositoryType {

}
