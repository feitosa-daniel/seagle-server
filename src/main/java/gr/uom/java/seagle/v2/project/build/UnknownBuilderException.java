package gr.uom.java.seagle.v2.project.build;

/**
 *
 * @author Daniel Feitosa
 */
public class UnknownBuilderException extends UnsupportedOperationException{

    /**
     * 
     * @param project Name of the project+version, or path to the project folder
     */
    public UnknownBuilderException(String project){
        super("Cannot identify a builder for "+ project);
    }
}
