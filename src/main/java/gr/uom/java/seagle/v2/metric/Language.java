/**
 *
 */
package gr.uom.java.seagle.v2.metric;

/**
 * @author Elvis Ligu
 * @version 0.0.1
 * @since 0.0.1
 */
public class Language {

   /**
    * Used for unspecified language, where there is no other applicable
    * language.
    */
   public final static String UNSPECIFIED = "UNSPECIFIED";
   /**
    * The Java programming language.
    */
   public final static String JAVA = "JAVA";
   /**
    * The C++ programming language.
    */
   public final static String CPP = "CPP";
   /**
    * The C programming language.
    */
   public final static String C = "C";
   /**
    * The PHP programming language.
    */
   public final static String PHP = "PHP";
   /**
    * The Javascript programming language.
    */
   public final static String JAVASCRIPT = "JAVASCRIPT";

   /**
    * Enumeration of programming languages.
    * <p>
    *
    * @author Elvis Ligu
    * @version 0.0.1
    * @since 0.0.1
    */
   public static enum Enum {

      /**
       * Used for unspecified language, where there is no other applicable
       * language.
       */
      UNSPECIFIED,
      /**
       * The Java programming language.
       */
      JAVA,
      /**
       * The C++ programming language.
       */
      CPP,
      /**
       * The C programming language.
       */
      C,
      /**
       * The PHP programming language.
       */
      PHP,
      /**
       * The Javascript programming language.
       */
      JAVASCRIPT
   }
}
