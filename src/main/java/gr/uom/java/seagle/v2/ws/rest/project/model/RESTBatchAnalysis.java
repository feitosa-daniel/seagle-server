/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.project.model;

import java.util.Collection;
import java.util.HashSet;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "analysis")
public class RESTBatchAnalysis {

   private String email;
   
   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   private Collection<String> urls;
   
   /**
    * 
    */
   public RESTBatchAnalysis() {
      this.urls = new HashSet<>();
   }

   public Collection<String> getUrls() {
      return urls;
   }

   public void setUrls(Collection<String> urls) {
      if(urls == null) {
         urls = new HashSet<>();
      }
      this.urls = urls;
   }
}
