/**
 * 
 */
package gr.uom.java.seagle.v2.ws.rest.metric.model;

import gr.uom.java.seagle.v2.db.persistence.Metric;
import gr.uom.se.util.validation.ArgsCheck;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Elvis Ligu
 */
@XmlRootElement(name = "metric")
public class RESTMetric {

   private String name;
   private String description;
   private String mnemonic;
   
   /**
    * 
    */
   public RESTMetric() {
   }
   
   public RESTMetric(Metric metric) {
      ArgsCheck.notNull("metric", metric);
      this.name = metric.getName();
      this.mnemonic = metric.getMnemonic();
      this.description = metric.getDescription();
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public String getMnemonic() {
      return mnemonic;
   }

   public void setMnemonic(String mnemonic) {
      this.mnemonic = mnemonic;
   }
}
