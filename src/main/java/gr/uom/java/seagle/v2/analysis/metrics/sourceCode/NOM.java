package gr.uom.java.seagle.v2.analysis.metrics.sourceCode;

import gr.uom.java.ast.ClassObject;
import gr.uom.java.ast.SystemObject;
import gr.uom.java.seagle.v2.SeagleManager;
import gr.uom.java.seagle.v2.analysis.metrics.aggregation.MetricAggregationStrategy;
import gr.uom.java.seagle.v2.analysis.project.evolution.JavaProject;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;

/**
 *
 * @author Thodoris Chaikalis
 */
public class NOM extends AbstractJavaExecutableMetric {

    public static final String MNEMONIC = "NOM";

    public NOM(SeagleManager seagleManager) {
        super(seagleManager);
    }

    @Override
    public void calculate(SystemObject systemObject, JavaProject javaProject) {
        ListIterator<ClassObject> classIterator = systemObject.getClassListIterator();
        Map<String, Double> valuePerClass = new LinkedHashMap<>();
        while (classIterator.hasNext()) {
            ClassObject classObject = classIterator.next();
            classNOM(valuePerClass,classObject);
        }
        storeValuesForAllNodesInMemory(getMnemonic(), valuePerClass, javaProject);
         storeProjectLevelAggregationMetricInMemory(getMnemonic(), javaProject, valuePerClass, MetricAggregationStrategy.Sum);
    }

    private void classNOM(Map<String, Double> valuePerClass, ClassObject classObject) {
        valuePerClass.put(classObject.getName(), (double)classObject.getNumberOfMethods());
    }

    @Override
    public String getMnemonic() {
        return MNEMONIC;
    }

    @Override
    public String getDescription() {
        return "Number of Methods for each class";
    }

    @Override
    public String getName() {
        return "Number of Methods";
    }

}
