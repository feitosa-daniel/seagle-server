/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.uom.java.seagle.v2.analysis.dpd.data;

/**
 *
 * @author Samet
 */
public class DPDPatternInstanceRole 
{
    private String name;
    private float similarityScore;
    private String element;

    public DPDPatternInstanceRole(String name, float similarityScore, String element) 
    {
        this.name = name;
        this.similarityScore = similarityScore;
        this.element = element;
    }
    
    /***
     * Gets the name of the instance role.
     * @return The name of the role.
     */
    public String getName()
    {
        return this.name;
    }
    
    /***
     * Gets the similarity score of the instance role.
     * Similarity score is a number between 0 and 1, which indicates how this
     * instance role similar to the original pattern instance role.If the value 
     * is 0, then the role has nothing in common with the original one. If it is 
     * 1, they are identical.
     * @return The similarity score for the instance role.
     */
    public float getSimilarityScore()
    {
        return this.similarityScore;
    }
    
    /**
     * Gets the name of the related element.
     * @return The name of the element which has a design pattern implementation
     * in it (e.g methods, constructors)
     */
    public String getElement()
    {
        return this.element;
    }
    
}
